import Grade from "../components/Grade";
import { useState } from "react";

import Button from '@mui/material/Button';

function GradePage() {

    const  [name, setName]= useState("")
    const  [id, setId]= useState("")
    const  [result, setResult]= useState("")
    
    const  [score, setScore]= useState("")
    const  [grade, setGrade]= useState("")

    function calGradePage() {
        let n = parseInt(score);
        let grade = n;
        setGrade(grade);

        if( score >= 90 ) {
            setResult("A")
        } else if( score >= 85 ) {
            setResult("B+");
        } else if( score >= 80 ) {
            setResult("B")
        } else if( score >= 75 ) {
            setResult("C+")
        } else if( score >= 70 ) {
            setResult("C")
        } else if( score >= 65 ) {
            setResult("D+")
        } else if( score >= 60 ) {
            setResult("D")
        } else {
            setResult("E")
        }
    }

    return (
    
        <div align="left">
            <div align="center">
                <hr/>
                ชื่อ-นามสกุล : <input type="text"
                                    value= { name }
                                    onChange={ (e) => { setName(e.target.value) ; } } />  <br />
                รหัสนักศึกษา : <input type="text" 
                                    value= { id }
                                    onChange={ (e) => { setId(e.target.value) ; } } />  <br />
                คะแนน : <input type="text" 
                                value= { score }
                                onChange={ (e) => { setScore(e.target.value) ; } } />  <br />
                 <br />
               <Button variant="contained" onClick={ ()=>{ calGradePage() } } > 
                    Calculate
               </Button> <br />
              



                <hr/>
                ผลการคำนวณ

                
                <Grade
                    name={ name } 
                    id={ id }
                    score={ score }
                    grade={ result }
                    
                />
            </div>
        </div>


    );
}
export default GradePage;
