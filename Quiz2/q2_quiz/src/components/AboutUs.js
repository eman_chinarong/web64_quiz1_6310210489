function AboutUs (props) {

    return (
        <div>
          <h2>จัดทำโดย : {props.name} </h2>
          <h3>รหัสนักศึกษา : {props.id}</h3>
          <h3>มหาวิทยาลัยสงขลานครินทร์</h3>
        </div>
    );
}
export default AboutUs;