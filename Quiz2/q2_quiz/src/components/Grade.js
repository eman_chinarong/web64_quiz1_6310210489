function Grade (props) {

    return( 

        <div>
            <h2>ชื่อ-นามสกุล : {props.name} </h2>
            <h3>รหัสนักศึกษา : {props.id} </h3>
            <h3>คะแนน : {props.score} </h3>
            <h3>เกรด: {props.grade}</h3>
        </div>

    );
}

export default Grade;