import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import GradePage from './pages/GradePage';
import Header from './components/Header.js';

import { Routes, Route, Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
        <Header />
        <Routes>
            <Route path="about" element={
                  <AboutUsPage />
             } />
            
            <Route path="/" element={
                  <GradePage />
            }/>
        </Routes>
    </div>
  );
}

export default App;
